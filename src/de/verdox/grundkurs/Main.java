package de.verdox.grundkurs;

public class Main extends SimpleJava{

    public static void main (String[] args){

        byte byte1 = 1;
        short short1 = 1;
        int int1 = 1;
        long long1 = 1;
        float float1 = 1.1f;
        double double1 = 1.1;

        boolean boolean1 = true;
        char character1 = 'a';
        String string1 = "abcc";

        // Primitive Datentypen
        // Ganze Zahlen

        // Byte
        // Belegt 8 Bits
        // -> 2^7 - 1

        byte b1 = Byte.MAX_VALUE; //127
        byte b2 = Byte.MIN_VALUE; //-127

        //----------------------------------

        // Short
        // Belegt 2 Bytes (16 Bits)
        // -> 2^15 - 1

        short s1 = Short.MAX_VALUE; //32767
        short s2 = Short.MIN_VALUE;//-32767

        //----------------------------------

        // Int
        // Belegt 4 Bytes (32 Bits)
        // -> 2^31 - 1

        int i1 = Integer.MAX_VALUE; //2147483647
        int i2 = Integer.MIN_VALUE; //-2147483647

        //----------------------------------

        // Long
        // Belegt 8 Bytes (64 Bits)
        // Kann über den Syntax "L" gekennzeichnet werden
        // -> 2^63 - 1

        long l1 = Long.MAX_VALUE; //9.223372e+18
        long l2 = Long.MIN_VALUE;//-9.223372e+18

        // Gleitkommatypen

        // Float
        // Belegt 4 Bytes
        // Genauigkeit auf 7 Stellen (einfache Genauigkeit)
        // Müssen über den Syntax "F" gekennzeichnet werden -> Sonst geht Compiler von Double Values aus

        float f1 = Float.MAX_VALUE;
        float f2 = Float.MIN_VALUE;

        // Double
        // Belegt 8 Bytes
        // Genauigkeit auf 15 Stellen (doppelte Genauigkeit)

        double d1 = Double.MAX_VALUE;
        double d2 = Double.MIN_VALUE;


        // Boolescher Wahrheitswert
        // Kann true oder false sein

        boolean bool1 = true;
        boolean bool2 = false;

        // Zeichenwerte
        // Benötigt 2 Bytes

        char firstChar = 'A';
        char secondChar = 65;
        System.out.println(firstChar);
        System.out.println(secondChar);


        // Casting
        int integer1 = 5;
        byte byte2 = 2;
        // implizites Casting ("automatisches Casting") weil int den größeren Wertebereich hat
        integer1 = byte2;
        // explizites Casting weil der Wertebereich von int eine echte Obermenge von byte ist
        byte2 = (byte) integer1;


        // Zeichenketten
        // Strings sind kein primitiver Datentyp -> Sondern eine Klasse

        String s = "abc";
        System.out.println(s);
        System.out.println(s.toUpperCase());
        System.out.println(s.charAt(0));

        // Hier endet die erste Lektion!
    }

}
